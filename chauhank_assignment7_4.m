load 'housing_data.mat'

%% PART A

% learn rtree with at least 20 observations per leaf 
rtree = fitrtree(Xtrain, ytrain, 'MinLeafSize', 20);

% visualize image of rtree
view(rtree, 'mode', 'graph');


%% PART B

% make prediction for given test point
y_pred = predict(rtree, [5, 18, 2.31, 1, 0.5440, 2, 64, 3.7, 1, 300, 15, 390, 10]);


%% PART C

% mean squared error of train and test data for each value of MinLeafSize
maes_train = zeros(25,1);
maes_test = zeros(25,1);

for min_leaf_size = 1:25
    
    % learn tree with current value of minimum leaf size on train data
    curr_rtree = fitrtree(Xtrain, ytrain, 'MinLeafSize', min_leaf_size);
    
    % make predictions on train data
    preds_train = predict(curr_rtree, Xtrain);
    
    % make predictions on test data
    preds_test = predict(curr_rtree, Xtest);

    % calculate mean squared error train data
    maes_train(min_leaf_size,1) = mean(abs(preds_train - ytrain));
    
    % calculate mean squared error test data
    maes_test(min_leaf_size,1) = mean(abs(preds_test - ytest));
    
end

% plot all the things!
plot(1:25, maes_train, '-o');
hold on;
plot(1:25, maes_test, '-o');

% set plot properties
legend('Train data', 'Test data');
ax = gca;
ax.Title.String = 'MAE vs Minimum Observations per Leaf';
ax.XLabel.String = 'Minimum Observations per Leaf';
ax.YLabel.String = 'Mean Absolute Error';
