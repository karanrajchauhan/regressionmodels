load 'linear_data.mat'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PART A %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% ESTIMATE OLS MODEL PARAMETERS

% n, number of data points
num_pts = size(xData,1);

% X - ux and y-uy, easier to calculate covariance matrices
X_centered = xData - mean(xData,1);
y_centered = yData - mean(yData,1);

% Estimates of Cov(X) and Cov(X,Y)
sigmaX = (1/num_pts) * (transpose(X_centered) * X_centered);
sigmaXY = (1/num_pts) * (transpose(X_centered) * y_centered);

% Estimates of w_ols and b_ols
w_ols = sigmaX \ sigmaXY;
b_ols = mean(yData,1) - (transpose(w_ols) * mean(xData,1));


%% PLOT OLS DATA

% plot training data 
figure;
scatter(xData, yData, 'filled');
hold on;

% 64 x values and calculated y values from ols parameters to plot
x = min(xData):(max(xData)-min(xData))/64:max(xData);
y = transpose(w_ols)*x + b_ols;
plot(x, y);
hold on;


%% CALCULATE OLS ERRORS

% estimates for each x
hx_ols = transpose(w_ols)*xData + b_ols;

% mean squared error
mse = mean((hx_ols - yData) .^ 2);

% mean absolute error
mae = mean(abs(hx_ols - yData));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PART B %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% PERFORM ROBUST LEAST SQUARES

% robust ls with different rho functions
% const parameter in 'on' by default
b_cauchy = robustfit(xData, yData, 'cauchy');
b_fair = robustfit(xData, yData, 'fair');
b_huber = robustfit(xData, yData, 'huber');
b_talwar = robustfit(xData, yData, 'talwar');


%% PLOT ROBUST LEAST SQUARES DATA

% x values and calculated y values from parameters to plot
x = min(xData):(max(xData)-min(xData))/64:max(xData);
x = vertcat(ones(1,size(x,2)), x);

y = transpose(b_cauchy)*x;
plot(x(2,:), y);
hold on;

y = transpose(b_fair)*x;
plot(x(2,:), y);
hold on;

y = transpose(b_huber)*x;
plot(x(2,:), y);
hold on;

y = transpose(b_talwar)*x;
plot(x(2,:), y);

% set plot properties
legend('Train Data', 'OLS', 'Cauchy', 'Fair', 'Huber', 'Talwar');
ax = gca;
ax.Title.String = 'OLS, Robust Least Squares with Varios Rho';
ax.XLabel.String = 'x';
ax.YLabel.String = 'h(x)';


%% CALCULATE ROBUST LEAST SQUARES ERRORS

% modify x so that we can matrix multiply parameters vector
xData = [ones(1,size(xData,1)); transpose(xData)];
yData = transpose(yData);

% mean square errors
mse_cauchy = mean((yData - transpose(b_cauchy)*xData) .^ 2);
mse_fair = mean((yData - transpose(b_fair)*xData) .^ 2);
mse_huber = mean((yData - transpose(b_huber)*xData) .^ 2);
mse_talwar = mean((yData - transpose(b_talwar)*xData) .^ 2);

% mean absolute errors
mae_cauchy = mean(abs(yData - transpose(b_cauchy)*xData));
mae_fair = mean(abs(yData - transpose(b_fair)*xData));
mae_huber = mean(abs(yData - transpose(b_huber)*xData));
mae_talwar = mean(abs(yData - transpose(b_talwar)*xData));
