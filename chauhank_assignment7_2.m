load 'quad_data.mat'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PART A %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% plot training data points
figure;
scatter(xtrain, ytrain, 'filled');
hold on


%% OLS WITH DEGREE 2, 6, 10, 14 POLYNOMIALS

% points to plot on graph
plot_x = transpose(min(xtrain):(max(xtrain)-min(xtrain))/100:max(xtrain));

% xtrain for degree 2
exponents = 0:2;
xtrain2 = xtrain .^ exponents;

% ols b's for degree 2
b2 = ridge(ytrain, xtrain2(:,2:end), 0, 0);

% plot the degree 2 polynomial
x = plot_x .^ exponents;
y2 = x * b2;
plot(x(:,2), y2, 'LineWidth', 1.25);
hold on

% xtrain for degree 6
exponents = 0:6;
xtrain6 = xtrain .^ exponents;

% ols b's for degree 6
b6 = ridge(ytrain, xtrain6(:,2:end), 0, 0);

% plot the degree 6 polynomial
x = plot_x .^ exponents;
y6 = x * b6;
plot(x(:,2), y6, 'LineWidth', 1.25);
hold on

% xtrain for degree 10
exponents = 0:10;
xtrain10 = xtrain .^ exponents;

% ols b's for degree 10
b10 = ridge(ytrain, xtrain10(:,2:end), 0, 0);

% plot the degree 10 polynomial
x = plot_x .^ exponents;
y10 = x * b10;
plot(x(:,2), y10, 'LineWidth', 1.25);
hold on

% xtrain for degree 14
exponents = 0:14;
xtrain14 = xtrain .^ exponents;

% ols b's for degree 14
b14 = ridge(ytrain, xtrain14(:,2:end), 0, 0);

% plot the degree 14 polynomial
x = plot_x .^ exponents;
y14 = x * b14;
plot(x(:,2), y14, 'LineWidth', 1.25);

% add legend and axes titles
legend('Training Data', 'Degree 2', 'Degree 6', 'Degree 10', 'Degree 14');

ax = gca;
ax.Title.String = 'Ordinary Least Square Polynomial Fitting';
ax.XLabel.String = 'x';
ax.YLabel.String = 'h(x)';
ax.LineWidth = 1.5;


%% CALCULATE MSE vs DEGREE OF POLYNOMIAL

% mse values for each degree for train data
train_mse_vals = zeros(14, 1);

% mse values for each degree for test data
test_mse_vals = zeros(14, 1);

% perform ols for each degree
for deg = 1:14
    
    % exponents to raise to for current degree
    deg_exponents = 0:deg;
    
    % xtrain for current degree
    deg_xtrain = xtrain .^ deg_exponents;

    % ols b's current degree
    deg_b = ridge(ytrain, deg_xtrain(:,2:end), 0, 0);
    
    % calculate current degree mse for train data
    train_mse_vals(deg, 1) = mean((ytrain - (deg_xtrain * deg_b)) .^ 2);
    
    % calculate current degree mse for test data
    test_mse_vals(deg, 1) = mean((ytest - ((xtest .^ deg_exponents) * deg_b)) .^ 2);
    
end

% plot the train and test mse values
figure;
plot(1:14, train_mse_vals, '-o');
hold on
plot(1:14, test_mse_vals, '-o');

% set plot properties
legend('Train data', 'Test data');
ax = gca;
ax.Title.String = 'Train and Test MSE values vs Polynomial Degree';
ax.XLabel.String = 'OLS polynomial degree';
ax.YLabel.String = 'Mean Square Error';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PART B %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% PART I - LOG LAMBDA vs MSE

% degree of polynomial for ols
poly_degree = 10;

% exponents to raise xtrain/xtest to
exponents = 0:poly_degree;

% train data for 10 deg polynomial
xtrain_deg = xtrain .^ exponents;

% mse values for each lambda for train data
train_mse_vals = zeros(31, 1);

% mse values for each lambda for test data
test_mse_vals = zeros(31, 1);

% perform ols for each lambda
for log_lambda = -25:5

    % ols b's for current log lambda
    b = ridge(ytrain, xtrain_deg(:,2:end), exp(log_lambda), 0);

    % calculate current degree mse for train data
    train_mse_vals(log_lambda+26, 1) = mean((ytrain - (xtrain_deg * b)) .^ 2);

    % calculate current degree mse for test data
    test_mse_vals(log_lambda+26, 1) = mean((ytest - ((xtest .^ exponents) * b)) .^ 2);

end

% plot MSE for train and test data against ln(lambda)
figure;
plot(-25:5, train_mse_vals, '-o');
hold on
plot(-25:5, test_mse_vals, '-o');

legend('Train data', 'Test data');
ax = gca;
ax.Title.String = 'Train and Test MSE values vs Log Lambda';
ax.XLabel.String = 'Log Lambda';
ax.YLabel.String = 'Mean Square Error';


%% PART II - VANILLA vs REGULARIZED OLS

% get index of lowest mse
[~, best_log_lambda] = min(test_mse_vals);

% get log lambda value from index
best_log_lambda = best_log_lambda - 26;

% get b values for best log(lambda), and unregularized ols
reg_b = ridge(ytrain, xtrain_deg(:,2:end), exp(best_log_lambda), 0);
b = ridge(ytrain, xtrain_deg(:,2:end), 0, 0);

% points to plot on graph
plot_x = transpose(min(xtrain):(max(xtrain)-min(xtrain))/100:max(xtrain));

% get y values for plot_x
x = plot_x .^ exponents;
y = x * b;
reg_y = x * reg_b;

% plot all the things!
figure;
scatter(xtrain, ytrain, 'filled');
hold on
plot(x(:,2), y, 'LineWidth', 1.25);
hold on
plot(x(:,2), reg_y, 'LineWidth', 1.25);

% set plot properties
legend('Train data', 'OLS', 'Regularized OLS with Least MSE');
ax = gca;
ax.Title.String = 'OLS and Regularized OLS on train data';
ax.XLabel.String = 'x';
ax.YLabel.String = 'h(x)';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PART B %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% RIDGE COEFFICIENTS AS FUNCTION OF LOG LAMBDA

% degree of polynomial for ols
poly_degree = 4;

% exponents to raise xtrain/xtest to
exponents = 0:poly_degree;

% train data for 4 deg polynomial
xtrain_deg = xtrain .^ exponents;

% b values for each lambda for train data
b_vals = zeros(31, 5);

% perform ols for each lambda
for log_lambda = -25:5

    % ols b's for current log lambda
    b = ridge(ytrain, xtrain_deg(:,2:end), exp(log_lambda), 0);

    % save the b values for current log lambda
    b_vals(log_lambda+26, :) = transpose(b);

end

% plot all the things!
figure;
plot(-25:5, b_vals(:,1), '-o');
hold on;
plot(-25:5, b_vals(:,2), '-o');
hold on;
plot(-25:5, b_vals(:,3), '-o');
hold on;
plot(-25:5, b_vals(:,4), '-o');
hold on;
plot(-25:5, b_vals(:,5), '-o');

% set plot properties
legend('w0', 'w1', 'w2', 'w3', 'w4');
ax = gca;
ax.Title.String = 'Regularized OLS coefficients vs Log Lambda';
ax.XLabel.String = 'Log Lambda';
ax.YLabel.String = 'Coefficient Value';
