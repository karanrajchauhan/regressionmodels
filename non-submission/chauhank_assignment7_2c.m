load 'quad_data.mat'

% degree of polynomial for ols
poly_degree = 4;

% exponents to raise xtrain/xtest to
exponents = 0:poly_degree;

% train data for 4 deg polynomial
xtrain_deg = xtrain .^ exponents;

% b values for each lambda for train data
b_vals = zeros(31, 5);

% perform ols for each lambda
for log_lambda = -25:5

    % ols b's for current log lambda
    b = ridge(ytrain, xtrain_deg(:,2:end), exp(log_lambda), 0);

    % save the b values for current log lambda
    b_vals(log_lambda+26, :) = transpose(b);

end

% plot all the things!
plot(-25:5, b_vals(:,1), 'LineWidth', 1.25);
hold on
plot(-25:5, b_vals(:,2), 'LineWidth', 1.25);
hold on
plot(-25:5, b_vals(:,3), 'LineWidth', 1.25);
hold on
plot(-25:5, b_vals(:,4), 'LineWidth', 1.25);
hold on
plot(-25:5, b_vals(:,5), 'LineWidth', 1.25);
hold on

% set plot properties
legend('w0', 'w1', 'w2', 'w3', 'w4');
ax = gca;
ax.Title.String = 'Regularized OLS coefficients vs Log Lambda';
ax.XLabel.String = 'Coefficient Value';
ax.YLabel.String = 'Log Lambda';
