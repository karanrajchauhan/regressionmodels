load 'quad_data.mat'

% plot data points
figure;
scatter(xtrain, ytrain, 'filled');
hold on


%% OLS WITH DEGREE 2, 6, 10, 14 POLYNOMIALS

% points to plot on graph
plot_x = transpose(min(xtrain):(max(xtrain)-min(xtrain))/100:max(xtrain));

% xtrain for degree 2
exponents = 0:2;
xtrain2 = xtrain .^ exponents;

% ols b's for degree 2
b2 = ridge(ytrain, xtrain2(:,2:end), 0, 0);

% plot the degree 2 polynomial
x = plot_x .^ exponents;
y2 = x * b2;
plot(x(:,2), y2, 'LineWidth', 1.5);
hold on

% xtrain for degree 6
exponents = 0:6;
xtrain6 = xtrain .^ exponents;

% ols b's for degree 6
b6 = ridge(ytrain, xtrain6(:,2:end), 0, 0);

% plot the degree 6 polynomial
x = plot_x .^ exponents;
y6 = x * b6;
plot(x(:,2), y6, 'LineWidth', 1.5);
hold on

% xtrain for degree 10
exponents = 0:10;
xtrain10 = xtrain .^ exponents;

% ols b's for degree 10
b10 = ridge(ytrain, xtrain10(:,2:end), 0, 0);

% plot the degree 10 polynomial
x = plot_x .^ exponents;
y10 = x * b10;
plot(x(:,2), y10, 'LineWidth', 1.5);
hold on

% xtrain for degree 14
exponents = 0:14;
xtrain14 = xtrain .^ exponents;

% ols b's for degree 14
b14 = ridge(ytrain, xtrain14(:,2:end), 0, 0);

% plot the degree 14 polynomial
x = plot_x .^ exponents;
y14 = x * b14;
plot(x(:,2), y14, 'LineWidth', 1.5);

% add legend and axes titles
legend('Data Point', 'Degree 2', 'Degree 6', 'Degree 10', 'Degree 14');

ax = gca;
ax.Title.String = 'Ordinary Least Square Polynomial Fitting';
ax.XLabel.String = 'x';
ax.YLabel.String = 'h(x)';
ax.LineWidth = 2;


%% CALCULATE MSE vs DEGREE OF POLYNOMIAL

% mse values for each degree for train data
train_mse_vals = zeros(14, 1);

% mse values for each degree for test data
test_mse_vals = zeros(14, 1);

% perform ols for each degree
for deg = 1:14
    
    % exponents to raise to for current degree
    deg_exponents = 0:deg;
    
    % xtrain for current degree
    deg_xtrain = xtrain .^ deg_exponents;

    % ols b's current degree
    deg_b = ridge(ytrain, deg_xtrain(:,2:end), 0, 0);
    
    % calculate current degree mse for train data
    train_mse_vals(deg, 1) = mean((ytrain - (deg_xtrain * deg_b)) .^ 2);
    
    % calculate current degree mse for test data
    test_mse_vals(deg, 1) = mean((ytest - ((xtest .^ deg_exponents) * deg_b)) .^ 2);
    
end

% plot the train and test mse values
figure;
scatter(1:14, train_mse_vals, 'filled');
hold on
scatter(1:14, test_mse_vals, 'filled');

% set plot properties
legend('Train data', 'Test data');
ax = gca;
ax.Title.String = 'Train and Test MSE values vs Polynomial Degree';
ax.XLabel.String = 'OLS polynomial degree';
ax.YLabel.String = 'Mean Square Error';
