load 'quad_data.mat'

%% PART I - LOG LAMBDA vs MSE

% degree of polynomial for ols
poly_degree = 10;

% exponents to raise xtrain/xtest to
exponents = 0:poly_degree;

% train data for 10 deg polynomial
xtrain_deg = xtrain .^ exponents;

% mse values for each lambda for train data
train_mse_vals = zeros(31, 1);

% mse values for each lambda for test data
test_mse_vals = zeros(31, 1);

% perform ols for each lambda
for log_lambda = -25:5

    % ols b's for current log lambda
    b = ridge(ytrain, xtrain_deg(:,2:end), exp(log_lambda), 0);

    % calculate current degree mse for train data
    train_mse_vals(log_lambda+26, 1) = mean((ytrain - (xtrain_deg * b)) .^ 2);

    % calculate current degree mse for test data
    test_mse_vals(log_lambda+26, 1) = mean((ytest - ((xtest .^ exponents) * b)) .^ 2);

end

% plot MSE for train and test data against ln(lambda)
figure;
scatter(-25:5, train_mse_vals, 'filled');
hold on
scatter(-25:5, test_mse_vals, 'filled');

legend('Train data', 'Test data');
ax = gca;
ax.Title.String = 'Train and Test MSE values vs Log Lambda';
ax.XLabel.String = 'Log Lambda';
ax.YLabel.String = 'Mean Square Error';


%% PART II - VANILLA vs REGULARIZED OLS

% get index of lowest mse
[~, best_log_lambda] = min(test_mse_vals);

% get log lambda value from index
best_log_lambda = best_log_lambda - 26;

% get b values for best log(lambda), and unregularized ols
reg_b = ridge(ytrain, xtrain_deg(:,2:end), exp(best_log_lambda), 0);
b = ridge(ytrain, xtrain_deg(:,2:end), 0, 0);

% points to plot on graph
plot_x = transpose(min(xtrain):(max(xtrain)-min(xtrain))/100:max(xtrain));

% get y values for plot_x
x = plot_x .^ exponents;
y = x * b;
reg_y = x * reg_b;

% plot all the things!
figure;
scatter(xtrain, ytrain, 'filled');
hold on
plot(x(:,2), y, 'LineWidth', 1.25);
hold on
plot(x(:,2), reg_y, 'LineWidth', 1.25);

% set plot properties
legend('Train data', 'OLS', 'Regularized OLS');
ax = gca;
ax.Title.String = 'OLS and Regularized OLS on train data';
ax.XLabel.String = 'h(x)';
ax.YLabel.String = 'x';
