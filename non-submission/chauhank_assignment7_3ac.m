load 'prostateStnd.mat';

%% INITIALIZE TO RIDGE COEFFICIENTS

num_pts = size(Xtrain,1);
num_features = size(Xtrain,2);

% log(lambda) for ridge
log_lambdas = -5:10;
lambdas = exp(log_lambdas);

num_lambdas = numel(log_lambdas);

% i,j is value of b_j for loglambda at index i
b_vals4lambdas = zeros(num_lambdas, num_features+1);

% initialize for each log lambda
for i = 1:num_lambdas
    
    % value of current log lambda
    curr_ll = log_lambdas(i);
    
    % get b ridge for current value of lambda
    b_vals4lambdas(i, :) = transpose(ridge(ytrain, Xtrain, exp(curr_ll), 0));

end

% plot for ridge regression
figure;
for i = 1:num_features
    plot(log_lambdas, b_vals4lambdas(:,1+i), '-o');
    hold on
end

% set plot properties
legend('feat1', 'feat2', 'feat3', 'feat4', 'feat5', 'feat6', 'feat7', 'feat8');
ax = gca;
ax.Title.String = 'Values of ridge coefficients of features vs Log Lambda';
ax.XLabel.String = 'Log Lambda';
ax.YLabel.String = 'Coefficient value';


%% PREDICT Y VALUES USING RIDGE PARAMETERS

% initialize
y_preds_train = zeros(16, size(Xtrain,1));
y_preds_test = zeros(16, size(Xtest,1));

% get predictions for parameters calculated from various lambda
for i = 1:num_lambdas
    
    y_preds_train(i, :) = b_vals4lambdas(i, :) * vertcat(ones(1,size(Xtrain,1)), transpose(Xtrain));
    y_preds_test(i, :) = b_vals4lambdas(i, :) * vertcat(ones(1,size(Xtest,1)), transpose(Xtest));
    
end


%% PLOT MSE FROM RIDGE

% calculate error
errors_train = y_preds_train - transpose(ytrain);
errors_test = y_preds_test - transpose(ytest);

mean_sq_errors_train = mean(errors_train .^ 2, 2);
mean_sq_errors_test = mean(errors_test .^ 2, 2);

% plot stuff
figure;
plot(log_lambdas, mean_sq_errors_train, '-o');
hold on
plot(log_lambdas, mean_sq_errors_test, '-o');

% set plot properties
legend('Train Data', 'Test Data');
ax = gca;
ax.Title.String = 'MSE vs Log Lambda for Ridge';
ax.XLabel.String = 'Log Lambda';
ax.YLabel.String = 'Mean Squared Error';


%% RUN SHOOTING ALGORITHM

% max number of iterations
max_iter = 100;

% X tilde, y tilde
X_centered = Xtrain - mean(Xtrain,1);
y_centered = ytrain - mean(ytrain,1);

for iter = 1:max_iter
    
    pre = b_vals4lambdas;
    
    % fix one feature and optimzie it
    for fix_feat_idx = 1:num_features
        
        a = 2 * sum(X_centered(:, fix_feat_idx) .^ 2);
        
        % first summation term in c
        c1 = repmat(transpose(y_centered) * X_centered(:, fix_feat_idx), 1, num_lambdas);

        % second summation term in c
        % i,j is wjTxi. first column is constant term values for 1
        c2 = sum(repmat(X_centered(:,fix_feat_idx), 1, num_lambdas) .* ...
            (X_centered * transpose(b_vals4lambdas(:, 2:end))));

        % third summation term in c
        c3 = transpose(repmat(b_vals4lambdas(:, 1+fix_feat_idx), 1, num_pts) * (X_centered(:, fix_feat_idx) .^ 2));
        
        c = 2 * (c1 - c2 + c3);
        
        % soft_thresh(c/a, lambdas/a);
        b_vals4lambdas(:, 1+fix_feat_idx) = transpose(sign(c/a) .* max(0, abs(c/a) - lambdas/a));

    end
    
    % if no change then it has converged
    if pre == b_vals4lambdas
        break;
    end

end

% plot how each feature varies with lambda
figure;
for i = 1:num_features
    plot(log_lambdas, b_vals4lambdas(:,1+i), '-o');
    hold on
end

% set plot properties
legend('feat1', 'feat2', 'feat3', 'feat4', 'feat5', 'feat6', 'feat7', 'feat8');
ax = gca;
ax.Title.String = 'Values of lasso coefficients of features vs Log Lambda';
ax.XLabel.String = 'Log Lambda';
ax.YLabel.String = 'Coefficient value';


%% PREDICT Y VALUES USING LASSO PARAMETERS

% initialize
y_preds_train = zeros(16, size(Xtrain,1));
y_preds_test = zeros(16, size(Xtest,1));

% get predictions for parameters calculated from various lambda
for i = 1:num_lambdas
    
    y_preds_train(i, :) = b_vals4lambdas(i, :) * vertcat(ones(1,size(Xtrain,1)), transpose(Xtrain));
    y_preds_test(i, :) = b_vals4lambdas(i, :) * vertcat(ones(1,size(Xtest,1)), transpose(Xtest));
    
end


%% PLOT MSE FROM LASSO

% calculate error
errors_train = y_preds_train - transpose(ytrain);
errors_test = y_preds_test - transpose(ytest);

mean_sq_errors_train = mean(errors_train .^ 2, 2);
mean_sq_errors_test = mean(errors_test .^ 2, 2);

% plot stuff
figure;
plot(log_lambdas, mean_sq_errors_train, '-o');
hold on
plot(log_lambdas, mean_sq_errors_test, '-o');

% set plot properties
legend('Train Data', 'Test Data');
ax = gca;
ax.Title.String = 'MSE vs Log Lambda for LASSO';
ax.XLabel.String = 'Log Lambda';
ax.YLabel.String = 'Mean Squared Error';
