load 'linear_data.mat'

%% PERFORM ROBUST LEAST SQUARES

% robust ls with different rho functions
% const parameter in 'on' by default
b_cauchy = robustfit(xData, yData, 'cauchy');
b_fair = robustfit(xData, yData, 'fair');
b_huber = robustfit(xData, yData, 'huber');
b_talwar = robustfit(xData, yData, 'talwar');
% b_fair = flipud(b_fair);

%% PLOT DATA AND ROBUST LS ESTIMATES

figure;

% plot training data
scatter(xData, yData, 'filled');
hold on;

% x values and calculated y values from parameters to plot
x = min(xData):(max(xData)-min(xData))/64:max(xData);
x = vertcat(ones(1,size(x,2)), x);

y = transpose(b_cauchy)*x;
plot(x(2,:), y);
hold on;

y = transpose(b_fair)*x;
plot(x(2,:), y);
hold on;

y = transpose(b_huber)*x;
plot(x(2,:), y);
hold on;

y = transpose(b_talwar)*x;
plot(x(2,:), y);

% set plot properties
legend('Train Data', 'Cauchy', 'Fair', 'Huber', 'Talwar');
ax = gca;
ax.Title.String = 'Robust Least Squares with Varios Rho';
ax.XLabel.String = 'x';
ax.YLabel.String = 'h(x)';


%% CALCULATE ERRORS

% modify x so that we can matrix multiply parameters vector
xData = [ones(1,size(xData,1)); transpose(xData)];
yData = transpose(yData);

% mean square errors
mse_cauchy = mean((yData - transpose(b_cauchy)*xData) .^ 2);
mse_fair = mean((yData - transpose(b_fair)*xData) .^ 2);
mse_huber = mean((yData - transpose(b_huber)*xData) .^ 2);
mse_talwar = mean((yData - transpose(b_talwar)*xData) .^ 2);

% mean absolute errors
mae_cauchy = mean(abs(yData - transpose(b_cauchy)*xData));
mae_fair = mean(abs(yData - transpose(b_fair)*xData));
mae_huber = mean(abs(yData - transpose(b_huber)*xData));
mae_talwar = mean(abs(yData - transpose(b_talwar)*xData));
